import { Component } from '@angular/core';
import { Router } from "@angular/router"
import {Http,Headers} from "@angular/http";

@Component({
  selector: 'registration',
  templateUrl: './registration.html',
  styleUrls: ['./registration.css']
})
export class registration {
  userCred={
    fName:"",
    LName:"",
    Pswrd:"",
    cPswrd:"",
    eMail:"",
  };

  constructor(private route:Router,private http:Http){

  }

  Registered()
  {
    if(this.userCred.Pswrd != this.userCred.cPswrd)
    {
      console.log("password does not match");
    }
    else{
      let hdr = new Headers();
      this.http.post("http://localhost:5009/register",this.userCred,{
        headers:hdr
      }).subscribe(data=>{
        console.log('Registered');
        console.log(this.userCred);
        this.route.navigate(["login"]);
      });
    }
  }
}

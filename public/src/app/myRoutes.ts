import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { password }   from '../password/password';
import { registration }      from '../registration/registration';
import { login }      from '../login/login';
import { profile }  from '../profile/profile';
import { dashboard }  from '../dashboard/dashboard';
import { logout }  from '../logout/logout';


const routes: Routes = [
  { path: '', redirectTo: '/vivek', pathMatch: 'full' },
  { path: 'registration',  component: registration },
  { path: 'password',     component: password },
  { path: 'login',     component: login },
  { path: 'profile',     component: profile },
  { path: 'dashboard',     component: dashboard },
  { path: 'logout',     component: logout }

];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}

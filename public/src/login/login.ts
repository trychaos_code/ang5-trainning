import { Component } from '@angular/core';

import { Router } from "@angular/router"
import {Headers, Http} from "@angular/http";
import {el} from "@angular/platform-browser/testing/src/browser_util";

@Component({
  selector: 'login',
  templateUrl: './login.html',
  styleUrls: ['./login.css']
})
export class login {
  userCred= {
    ID: "",
    pswd: "",
  };

  constructor(private router:Router, private http:Http){

  }

loggedIn(){
  console.log('Wait while you are being logged in');
  console.log(this.userCred);
  let hdr = new Headers();
  this.http.post("http://localhost:5009/login",this.userCred,{
    headers:hdr
  }).subscribe(data=>{
    let resp = data.json();
    if(resp.code===200){
      this.router.navigateByUrl('dashboard');
    }else {
      alert("User not found");
    }
  });
}
reg(){
this.router.navigateByUrl('registration')
}
}
